const webpack = require("webpack");
const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const entryDir = "./src/index.js";
const outputDir = path.resolve(__dirname, "dist");

const config = {
    entry: entryDir,
    output: {
        path: outputDir,
        filename: "bundle.js"
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: "vue-loader"
        },
        {
            test: /\.css/,
            use: [
              "vue-style-loader",
              "css-loader"
            ]
        }]
    },
    resolve: {
        extensions: [
        ".js",
        ".vue"
        ]
    },
    plugins: [new VueLoaderPlugin()],
    devServer: {
        static: {
          directory: outputDir
        },
        port: 5000
    }
};

module.exports = config;