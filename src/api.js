const endPoint = "https://swop.cx/graphql";
const API_KEY = "56132eaead48a4add8692447428d48359254d9c182f489817543586e02fba73e";

export default function API(query) {
    return fetch(endPoint, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": `ApiKey ${API_KEY}`
        },
        body: JSON.stringify({query}),
    })
    .then(res => res.json())
    .then(json => json.data);
}